class puma {
  if (!defined(Package['puma'])) {
    package {"puma":
      ensure   => present,
      provider => 'gem',
    }
  }
  if (!defined(Package['bundler'])) {
    package {'bundler':
      ensure   => present,
      provider => 'gem',
    }
  }
}

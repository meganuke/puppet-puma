define puma::app(
  $app_name    = $name,
  $app_user    = 'deployment',
  $app_root    = "/srv/${name}",
  $use_bundler = false,
) {
  user { $app_user:
    ensure   => present,
    shell    => '/bin/bash',
    password => '*',
    home     => "/home/${app_user}",
    system   => true,
  }
  group { $app_user:
    ensure  => present,
    system  => true,
    require => User[${app_user}],
  }

  file { [$app_root,
          "${app_root}/current",
          "${app_root}/shared",
          "${app_root}/shared/log",
          "${app_root}/shared/tmp",
          "${app_root}/shared/config",
          "${app_root}/shared/tmp/sockets",
          "${app_root}/shared/tmp/pids",
          "/home/${app_user}"]:
    ensure  => directory,
    owner   => $app_user,
    group   => $app_user,
    mode    => '0775',
    require => Group[${app_user}],
  }

  file { "${app_root}/shared/config/puma.rb":
    content => template('puma/puma.rb.erb'),
    owner   => $app_user,
    group   => $app_user,
    require => File ["${app_root}/shared/config"],
  }

  file {"/etc/init/${app_name}.conf":
    content => template('puma/init.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package['puma'],
  }

  #  service { $app_name:i
}
